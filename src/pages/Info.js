import { Fragment } from 'react';
import { Row, Col } from 'react-bootstrap'
export default function Info(){
  return(
    <Fragment>
      <Row>
        <Col className="px-5 pt-5">
          <h1>Contact Us</h1>
          
          
          <p>Your comments and suggestions are important to us. Reach out to Paimon's Bargains through the communication channels listed below.</p>
        </Col>
      </Row>
      <hr/>
      <Row className="p-auto">
        <Col className="px-5">
          <p className="fw-bold">Email:</p><p>paimon@gmail.com</p>
          <p className="fw-bold">Mobile:</p><p>+63 912 345 6789</p>
          <p className="fw-bold">Location:</p><p>Mondstadt, Teyvat</p>
        </Col>
      </Row>
      <hr/>

    </Fragment>
  )
}