import { Fragment } from 'react';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){
  const data = {
    title: "Welcome to Paimon's Bargains!",
    content: "The Traveller's Best Companion at your service.",
    destination: "/products",
    label: "View Shop"
  }
  return(
    <Fragment>
      <Banner data={data} />
      <Highlights />
    </Fragment>
  )
}