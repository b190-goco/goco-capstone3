import { Fragment, useEffect, useState, useContext } from 'react';
// import AdminView from '../components/AdminView';
// // import coursesData from '../data/coursesData';
// import UserView from '../components/UserView';
import OrderView from '../components/OrderView';
import OrderViewAdmin from '../components/OrderViewAdmin';
import UserContext from '../UserContext';

export default function Order() {

    // Checks to see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    const { user } = useContext(UserContext);

    // State that will be used to store the courses retrieved from the database
    const [courses, setCourses] = useState([]);

    //Function to fetch our courses data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
        const fetchData = () => {

            if (user.isAdmin !== true){
                fetch(`${ process.env.REACT_APP_API_URL }/users/orders`, {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${ localStorage.getItem('token') }`,
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => res.json())
                .then(data => {
                    let reverseOrder = []; let j=0
                    for(let i=data.length-1;i>=0;i--){
                        reverseOrder[j]=data[i];
                        j++;
                    }

                    setCourses(reverseOrder);

                });

                console.log(courses)
            }
            if (user.isAdmin === true){
                    fetch(`${ process.env.REACT_APP_API_URL }/users/allOrders`, {
                        method: 'GET',
                        headers: {
                            Authorization: `Bearer ${ localStorage.getItem('token') }`,
                            'Content-Type': 'application/json'
                        }
                    })
                .then(res => res.json())
                .then(data => {
                    let reverseOrder = []; let j=0
                    for(let i=data.length-1;i>=0;i--){
                        reverseOrder[j]=data[i];
                        j++;
                    }

                    setCourses(reverseOrder);

                });

                console.log(courses)
            }
                // fetch(`${ process.env.REACT_APP_API_URL}/users/cart/view`)
        }

    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {user.isAdmin!==true
            ?
            <h1 className="p-3">Your Orders</h1>
            :
            <h1 className="p-3">Order History</h1>
            }
            {user.isAdmin!==true
            ?
            <OrderView coursesData={courses} fetchData={fetchData}/>
            :
            <OrderViewAdmin coursesData={courses} fetchData={fetchData}/>
            }
        </Fragment>
    )

}