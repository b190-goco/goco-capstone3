const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Praesent sed sagittis ante. Cras justo libero, varius non ligula sit amet, venenatis auctor urna. Vivamus interdum ipsum nisl, ac congue sapien finibus ut. Nunc ut dui auctor, blandit dui vel, accumsan diam. Mauris eu quam placerat odio posuere euismod. Cras pellentesque arcu eu ultricies congue.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Praesent sed sagittis ante. Cras justo libero, varius non ligula sit amet, venenatis auctor urna. Vivamus interdum ipsum nisl, ac congue sapien finibus ut. Nunc ut dui auctor, blandit dui vel, accumsan diam. Mauris eu quam placerat odio posuere euismod. Cras pellentesque arcu eu ultricies congue.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Praesent sed sagittis ante. Cras justo libero, varius non ligula sit amet, venenatis auctor urna. Vivamus interdum ipsum nisl, ac congue sapien finibus ut. Nunc ut dui auctor, blandit dui vel, accumsan diam. Mauris eu quam placerat odio posuere euismod. Cras pellentesque arcu eu ultricies congue.",
		price: 40000,
		onOffer: true
	},
]

export default coursesData;