import {useState, useEffect} from 'react';

import Container from 'react-bootstrap/Container'

export default function Counter(){
	const [counter, setCounter] = useState(0);

	useEffect(()=>{
		document.title `You clicked ${counter} times`
		console.log(counter)
	},[])

	return(
		<Container>
			<p>You clicked ${counter} times</p>
			<button onClick = {()=>setCounter(counter+1)}>Click Me!</button>
		</Container>
		)
}