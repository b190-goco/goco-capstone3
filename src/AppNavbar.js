import { useContext, Fragment } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext';

export default function AppNavbar(){
	/*
		localStorage.getItem is used  to get a piece of information from the browser's local storage. devs usually insert information in the local storage for specific purposes; one of commonly used purpose is for login in. it accepts 1 argument pertaining to the specific key/property inside the local storage to get its value
	*/
	// uses the state hook to set the initial value for user variable coming from the local storage, specifically the email property
	const { user } = useContext(UserContext);
	console.log(user);

	return(
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
		{(user.id !== null) ?
		      <Container>
		        <Navbar.Brand href="/">Paimon's Bargains</Navbar.Brand>
		        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		        <Navbar.Collapse id="responsive-navbar-nav">
		          <Nav className="me-auto">
		            <Nav.Link href="/products/">Products</Nav.Link>
			            {(user.isAdmin === false) ?
				            <NavDropdown title="Profile" id="collasible-nav-dropdown">
					            <NavDropdown.Item href="/users/cart">Your Cart</NavDropdown.Item>
					            <NavDropdown.Item href="/users/orders">Your Orders</NavDropdown.Item>
					            <NavDropdown.Divider />
								<NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
							</NavDropdown>
							:
							<NavDropdown title="Profile" id="collasible-nav-dropdown">
					            <NavDropdown.Item href="/users/orders">All Orders</NavDropdown.Item>
					            <NavDropdown.Item href="/users/all">Set New Admin</NavDropdown.Item>
					            <NavDropdown.Divider />
								<NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
							</NavDropdown>
						}

		          </Nav>
		          <Nav>
		            <Nav.Link href="/info">Contact Us</Nav.Link>
		          </Nav>	
		        </Navbar.Collapse>
		      </Container>
		      :
		      <Container>
		        <Navbar.Brand href="/">Paimon's Bargains</Navbar.Brand>
		        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		        <Navbar.Collapse id="responsive-navbar-nav">
		          <Nav className="me-auto">
		            <Nav.Link href="/products/">Products</Nav.Link>
		            <Nav.Link className="px-3"href="/login">Login</Nav.Link>
		            <Nav.Link className=""href="/register">Register</Nav.Link>
		          </Nav>
		          <Nav>
		            <Nav.Link href="/info">Contact Us</Nav.Link>
		          </Nav>
		        </Navbar.Collapse>
		      </Container>

		}
		    </Navbar>
		)
}

/*
			<Navbar bg="dark" expand="lg">
				<Navbar.Brand className="mx-3 text-light" as={ Link } to='/'>Paimon's Gift Shop</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link className = "text-light" as={ NavLink } to='/' exact>Home</Nav.Link>
						<Nav.Link className = "text-light" as={ NavLink } to='/courses' exact> Products </Nav.Link>
						{(user.id !== null) ?
							<Nav.Link className = "text-light" as={ NavLink } to='/logout' exact> Logout</Nav.Link>
						:
							<Fragment>
								<Nav.Link className = "text-light ml-auto" as={ NavLink } to='/login' exact> Login</Nav.Link>
								<Nav.Link className = "text-light" as={ NavLink } to='/register' exact> Register</Nav.Link>
							</Fragment>
						}
					</Nav>
					<Nav>
					  <Nav.Link href="#deets">More deets</Nav.Link>
					  <Nav.Link eventKey={2} href="#memes">
					    Dank memes
					  </Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>*/