import { Row, Col, Card } from 'react-bootstrap';
// ctrl + shift + p
export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Blessing of the Welkin Moon</h2>
						</Card.Title>
						<Card.Text>For P249, get the Blessing of the Welkin Moon for an instant 300 Genesis Crystals and daily 90 Primogems for 30 days.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Starglitter Exchange</h2>
						</Card.Title>
						<Card.Text>Wishing duplicate characters gives you Starglitter. Exchange 25 for an exclusive 4-Star Weapon or 37 for a 4-Star Character.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Wish using Primogems</h2>
						</Card.Title>
						<Card.Text>Exhange 160 of your hard-earned Primogems to buy an Acquaint Fate for the Standard Banner or Intertwined Fate in the Limited Character or Weapon Banner.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}