import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useNavigate, Link, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Fragment } from 'react';
import { Table, Form } from 'react-bootstrap';

import UserContext from '../UserContext';

export default function CourseView(){
	const { productId } = useParams();
	const { user } = useContext(UserContext);

	const navigate = useNavigate();
	

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ stock, setStock ] = useState(0);
	const [ quantity, setQuantity ] = useState(0);

	const addToCart = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/addtocart`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: quantity
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title: "Successfully added",
					icon: "success",
					text: "You have successfully added this product to your cart."
				});
				setQuantity(0);
				navigate('/products');
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stock);
		})
	},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>

							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Card.Subtitle>Stock</Card.Subtitle>
							<Card.Text>{stock}</Card.Text>
							<Form  className="pb-3">
								<Form.Group controlId="orderQuantity">
									<Form.Label>Quantity</Form.Label>
									<Form.Control type="number" value={quantity}  onChange={e => setQuantity(e.target.value)} required/>
								</Form.Group>
							</Form>

							{ (user.id !== null)?
							<Button variant="primary" onClick={()=>addToCart(productId)} block>Add to Cart</Button>
							:
							<Link className='btn btn-danger btn-block' to='/login'>Login to buy</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)



}