import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartView(props){

    console.log(props);
    // Destructure our courses data from the props being passed by the parent component (courses page)
    // Includes the "fetchData" function that retrieves the courses from our database
    const { coursesData, fetchData } = props;

    // States for form inputs
    const [courseId, setCourseId] = useState("");
    const [courses, setCourses] = useState([]);
    
    const [cart, setCart] = useState([]);
    
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);

    // States to open/close modals
    const [showEdit, setShowEdit] = useState(false);
    const [showAdd, setShowAdd] = useState(false);
    const [showEmpty, setShowEmpty] = useState(false);

    // Functions to toggle the opening and closing of the "Add Course" modal
    const openAdd = () => setShowAdd(true);
    const closeAdd = () => setShowAdd(false);

    const openEmpty = () => setShowEmpty(true);
    const closeEmpty = () => setShowEmpty(false);



    /* 
    Function to open the "Edit Course" modal:
        - Fetches the selected course data using the course ID
        - Populates the values of the input fields in the modal form
        - Opens the "Edit Course" modal
    */
    const openEdit = (courseId) => {

        // Fetches the selected course data using the course ID
        fetch(`${ process.env.REACT_APP_API_URL}/products/${ courseId }`)
        .then(res => res.json())
        .then(data => {

            // console.log(data);

            // Changes the states for binded to the input fields
            // Populates the values of the input files in the modal form
            setCourseId(data._id);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStock(data.stock);
        })

        // Opens the "Edit Course" modal
        setShowEdit(true);
    };

    /* 
    Function to close our "Edit Course" modal:
        - Reset from states back to their initial values
        - Empties the input fields in the form whenever the modal is opened for adding a course
    */
    const closeEdit = () => {

        setShowEdit(false);
        setName("");
        setDescription("");
        setPrice(0);
        setStock(0);

    };

    const checkout = (e) => {

        // Prevent the form from redirecting to a different page on submit 
        // Helps retain the data if adding a course is unsuccessful
        e.preventDefault()

        fetch(`${ process.env.REACT_APP_API_URL }/users/checkout`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            // If the new course is successfully added
            if (data === true) {

                // Invoke the "fetchData" function passed from our parent component (courses page)
                // Rerenders the page because of the "useEffect"
                fetchData();

                // Show a success message via sweet alert
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Thank you for your purchase."                  
                })

                // Reset all states to their initial values
                // Provides better user experience by clearing all the input fieles when the user adds another course
                setCart([data]);

                setName("")
                setDescription("")
                setPrice(0)

                // Close the modal
                closeAdd();

            } else {

                fetchData();

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })
    }

    const empty = (e) => {

        // Prevent the form from redirecting to a different page on submit 
        // Helps retain the data if adding a course is unsuccessful
        e.preventDefault()

        fetch(`${ process.env.REACT_APP_API_URL }/users/cart/empty`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            }
        })
        .then(res => res.json())
        .then(data => {

            // If the new course is successfully added
            if (data === true) {

                // Invoke the "fetchData" function passed from our parent component (courses page)
                // Rerenders the page because of the "useEffect"
                fetchData();

                // Show a success message via sweet alert
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Cart successfully emptied."                  
                })

                // Reset all states to their initial values
                // Provides better user experience by clearing all the input fieles when the user adds another course
                // Close the modal
                closeEmpty();

            } else {

                fetchData();

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })
    }

    const editCourse = (e, courseId) => {
        
        e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/cart/${ courseId }/remove`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {

            if (data === true) {

                fetchData();

                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Item successfully removed."
                });

                closeEdit();

            } else {

                fetchData();

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                });

            }

        })
    }

    // Map through the courses received from the parent component (course page)
    // Re-renders the table whenever the "coursesData" is updated by adding, editing and deleting a course
    useEffect(() => {

        // const archiveToggle = (courseId, isActive) => {

        //     console.log(!isActive);

        //     fetch(`${ process.env.REACT_APP_API_URL }/products/${ courseId }/archive`, {
        //         method: 'PUT',
        //         headers: {
        //             "Content-Type": "application/json",
        //             Authorization: `Bearer ${ localStorage.getItem('token') }`
        //         },
        //         body: JSON.stringify({
        //             isActive: !isActive
        //         })
        //     })
        //     .then(res => res.json())
        //     .then(data => {

        //         if (data === true) {

        //             fetchData();

        //             Swal.fire({
        //                 title: "Success",
        //                 icon: "success",
        //                 text: "Course successfully archived/unarchived."
        //             });

        //         } else {

        //             fetchData();

        //             Swal.fire({
        //                 title: "Something went wrong",
        //                 icon: "error",
        //                 text: "Please try again."
        //             });

        //         }
        //     })
        // }
        // console.log(coursesData)
        const coursesArr = coursesData.map(course => {

            return(

                <tr key={course.productId}>
                    <td>{course.name}</td>
                    <td>{course.quantity}</td>
                    {/*<td>{course.stock}</td>*/}
                    <td>₱ {course.price}</td>
                    {/*<td>
                         
                            - If the course's "isActive" field is "true" displays "available"
                            - Else if the course's "isActive" field is "false" displays "unavailable"
                        
                        {course.isActive
                            ? <span>Available</span>
                            : <span>Unavailable</span>
                        }
                    </td>*/}
                    <td>
                        <Button
                            variant="warning"
                            size="sm"
                            onClick={() => openEdit(course.productId)}
                        >
                            Remove Item
                        </Button>
                        {/* 
                            - Display a red "Disable" button if course is "active"
                            - Else display a green "Enable" button if course is "inactive"
                        */}
                     {/*   {course.isActive
                            ?
                            <Button 
                                variant="danger" 
                                size="sm" 
                                onClick={() => archiveToggle(course._id, course.isActive)}
                            >
                                Disable
                            </Button>
                            :
                            <Button 
                                variant="success"
                                size="sm"
                                onClick={() => archiveToggle(course._id, course.isActive)}
                            >
                                Enable
                            </Button>
                        }*/}
                    </td>
                </tr>

            )

        });

        // Set the "courses" state with the table rows returned by the map function
        // Renders table row elements inside the table via this "AdminView" return statement below
        setCourses(coursesArr);

    }, [coursesData, fetchData]);

    return(
        <Fragment>
            {coursesData.length !== 0
            ?    
            <div className="text-center my-4 px-3">
                <h2>Your Cart</h2>
            </div>
            :
            <div className="text-left my-4 px-3">
                <h2>Your Cart is Empty</h2>
            </div>
            }
            {coursesData.length !== 0
            ?
            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white">
                    <tr>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {courses}
                </tbody>
            </Table>
            :
            <div className="text-left my-4 px-3">
                <p>It seems you have not added products to your cart yet.</p>
            </div>
            }
            {coursesData.length !== 0
            ?
            <div className="d-flex justify-content-center">
                <Button className="mx-5" variant="success" onClick={openAdd}>Checkout</Button>        
                <Button variant="danger" onClick={openEmpty}>Empty Cart</Button>         
            </div>
            :
            <div className="d-flex justify-content-center">
                <Button className="" variant="primary" href='/products'>Go back to Shop</Button>                 
            </div>
            }
            {/*ADD MODAL*/}
            {/*<Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => addCourse(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="courseName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="courseDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="coursePrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
                        </Form.Group>
                        <Form.Group controlId="courseStock">
                            <Form.Label>Stock</Form.Label>
                            <Form.Control type="number" value={stock}  onChange={e => setStock(e.target.value)} required/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
*/}
            {/*EDIT MODAL*/}
            <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editCourse(e, courseId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <p>This item will be removed from your cart. Proceed?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning" onClick={closeEdit}>Cancel</Button>
                        <Button variant="success" type="submit">Continue</Button>
                    </Modal.Footer>
                </Form>
            </Modal>

            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => checkout(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <p>All the items listed will be included in the purchase. Proceed?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning" onClick={closeEdit}>Cancel</Button>
                        <Button variant="success" type="submit">Checkout</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            
            <Modal show={showEmpty} onHide={closeEmpty}>
                <Form onSubmit={e => empty(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure?</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <p>All the items will be removed from your cart. Proceed?</p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="warning" onClick={closeEmpty}>Cancel</Button>
                        <Button variant="success" type="submit">Empty Cart</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
            
        </Fragment>
    )
}
